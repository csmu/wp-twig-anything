var TwigAnythingDataSource_mysql = React.createClass({
    getInitialState: function() {
        return {
            mysql: '',
            mysql_type: 'get_results' // get_col, get_row, get_var
        }
    },
    getSettings: function() {
        return {
            mysql: this.state.mysql,
            mysql_type: this.state.mysql_type
        }
    },
    handleMysqlChange: function(e) {
        this.setState({mysql: e.target.value});
    },
    handleMysqlTypeChange: function(e) {
        this.setState({mysql_type: e.target.value});
    },
    render: function() {
        return (
            <table className="form-table">
                <tbody>

                {/* MYSQL QUERY TYPE */}
                <tr>
                    <th scope="row">
                        <label htmlFor="twig_anything_data_source_mysql_type">MySQL Result Type</label>
                    </th>
                    <td>
                        <select
                            name     = "twig_anything_data_source_mysql_type"
                            id       = "twig_anything_data_source_mysql_type"
                            value    = {this.state.mysql_type}
                            onChange = {this.handleMysqlTypeChange}>
                            <option value="get_results">The entire query result (get_results)</option>
                            <option value="get_col">A one dimensional array for a particular column (get_col)</option>
                            <option value="get_row">A single row array (get_row)</option>
                            <option value="get_var">A single value (get_var)</option>
                        </select>
                        <p className="description">
                            See <a href="https://codex.wordpress.org/Class_Reference/wpdb#SELECT_a_Variable" target="_blank">WordPress documentation</a> for more details.
                        </p>
                    </td>
                </tr>

                {/* MYSQL QUERY */}
                <tr>
                    <th scope="row">
                        <label htmlFor="twig_anything_data_source_mysql">MySQL Query</label>
                    </th>
                    <td>
                        <textarea
                            name     = "twig_anything_data_source_mysql"
                            id       = "twig_anything_data_source_mysql"
                            value    = {this.state.mysql}
                            onChange = {this.handleMysqlChange}
                            className = "large-text"
                            style     = {{height: '14em'}} />
                        <p>
                            <code>{ '{{ wp_globals.wpdb.posts }}' }</code>
                            gets the correct name of your blog's <em>posts</em> table.
                            See&nbsp;
                            <a target="_blank" href="https://codex.wordpress.org/Class_Reference/wpdb#Tables">
                                the full list of WordPress tables.
                            </a>
                            <br/>
                            All&nbsp;
                            <a target="_blank" href="https://codex.wordpress.org/Global_Variables">
                                WordPress globals
                            </a>
                            &nbsp;are available, e.g. use <code>{ '{{ wp_globals.post.ID }}' }</code>
                            to get the current post ID in the posts loop.
                            <br/>
                            <code>{ '{{ get_the_ID() }}' }</code>
                            &nbsp;retrieves the numeric ID of the current post (
                            <a target="_blank" href="https://codex.wordpress.org/Function_Reference/get_the_ID">
                                read more
                            </a>
                            )
                            <br/>
                            <code>{ '{{ get_current_blog_id() }}' }</code>
                            &nbsp;retrieves the current blog ID (
                            <a target="_blank" href="https://codex.wordpress.org/Function_Reference/get_current_blog_id">
                                read more
                            </a>
                            )
                            <br/>
                            <code>{ '{{ wp_get_current_user() }}' }</code>
                            &nbsp;retrieves the current user object WP_user (
                            <a target="_blank" href="https://codex.wordpress.org/Function_Reference/wp_get_current_user">
                                read more
                            </a>
                            )
                        </p>
                    </td>
                </tr>

                </tbody>
            </table>
        )
    }
});