// While inside the wrapper, `this` refers to the function
// and is what was initialized, so we now want to export it.
exports.CodeMirror = this.CodeMirror;