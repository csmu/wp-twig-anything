// Make a local variable, so that addons can use it (instead of a global).
// The "this" variable, when wrapped with uglifyjs, will refer to the wrapper function.
// When the main CodeMirror library initializes, it uses `this` as a global,
// then sets this.CodeMirror, so with the wrapper
// we basically provide our own `this`.
// The local variable is to prevent mods using global CodeMirror variable.
var CodeMirror = this.CodeMirror;